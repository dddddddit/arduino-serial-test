import glob
import random
import unittest

import serial


class SerialTestCase(unittest.TestCase):
    def setUp(self):
        self.device = glob.glob('/dev/ttyACM*')[-1]

    def test10000(self):
        ser = serial.Serial(self.device, baudrate=9600, timeout=1, writeTimeout=5)

        p = list(random.randrange(128) for j in range(100))
        try:
            print('Written %s bytes' % ser.write(p))
        except Exception:
            pass
        for r in p:
            print(end='.', flush=True)
            self.assertEqual(r, ord(ser.read(1)))


def main():
    dev = glob.glob('/dev/ttyACM*')[-1]
    ser = serial.Serial(dev, baudrate=115200, timeout=1, writeTimeout=3)
    rnd = [random.randrange(256) for r in range(1000)]
    ser.write(rnd)
    for n, b in enumerate(rnd):
        read = ser.read(1)
        if not read:
            print('{:3d}  {:3d} != [empty string]'.format(
                n,
                b,
            ))
            break
        get = ord(read)
        print('{:3d}  {:3d} {}= {:3d}'.format(
            n,
            b,
            '=' if b == get else '!',
            get,
        ))
        if get != b:
            break


if __name__ == '__main__':
    # unittest.main()
    main()
